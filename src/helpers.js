export const calcularTotal = (cantidad,plazo) =>{

    let totalCantidad;
    if(cantidad <100000){
        totalCantidad = cantidad * .25;
    }else if(cantidad<500000 && cantidad >100000){
        totalCantidad = cantidad * .20;
    }else if (cantidad > 50000 && cantidad< 1000000){
        totalCantidad = cantidad * .15;
    }else{
        totalCantidad = cantidad * .1;
    }

    // calcular el plazo
    let totalPlazo =0;
    
    switch (plazo) {
        case 3:
            totalPlazo = cantidad *.05
            break;
        case 6:
            totalPlazo = cantidad *.10
            break;
        case 12:
            totalPlazo = cantidad *.15
            break;
        case 24:
            totalPlazo = cantidad *.20
            break;
    
        default:
            break;
    }

    return totalCantidad + totalPlazo + cantidad;
    
}