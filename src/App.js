import React, { Fragment, useState } from 'react';
import Hedaer from './components/Header';
import Form from './components/form';
import Mensaje from './components/Mensaje';
import Resultado from './components/Resultado';
import Spinner from './components/Spinner';



function App() {
    const [cantidad, guardarCAntidad] = useState(0);
    const [plazo, guardarPlazo] = useState('');
    const [total, guardarTotal] = useState(0);
    const [cargando, guardarCargando] = useState(false);
    let componente;

    if (cargando) {
        componente = <Spinner />
    }else if (total === 0 || isNaN(plazo) || isNaN(cantidad)) {
        componente = <Mensaje />
        const me = document.querySelector('.mensajes');
        const re = document.querySelector('resultado');
        if(re){
            me.removeChild(me.firstChild);
        }
        
    } else {
        componente = <Resultado
            total={total}
            plazo={plazo}
            cantidad={cantidad}
        />
    }

    
    return (
        <Fragment>
            <Hedaer
                titulo="Cotizador de Prestamos"
            />
            <div className="container">
                <Form
                    cantidad={cantidad}
                    guardarCAntidad={guardarCAntidad}
                    plazo={plazo}
                    guardarPlazo={guardarPlazo}
                    guardarTotal={guardarTotal}
                    guardarCargando={guardarCargando}
                />
                <div className="mensajes">
                    {componente}
                </div>

            </div>
        </Fragment>
    );
}

export default App;
