import React from 'react';

const Resultado = ({cantidad,total,plazo}) => {
    return (
        <div className="u-fullwidth resultado">
            <h2>Resumen</h2>
    <p>La cantidd solicitada es: ${cantidad}</p>
    <p>El plazo para pagar es: ${plazo} meses</p>
    <p>El monto mensual es: ${Math.round(total/plazo)}</p>
    <p>El monto total es: ${total}</p>
        </div>
    )
}

export default Resultado;